# SQUASH #

A competitive two player game created over a two day code jam.

## CONTROLS ##

Player I/Player II:

W/U jump

S/J SQUASH

A/H left

D/K right

## SCREENSHOTS ##

![](https://bitbucket.org/dseyedm/squash/downloads/battle.png)

![battle_won.png](https://bitbucket.org/dseyedm/squash/downloads/battle_won.png)