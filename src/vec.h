#pragma once

#include <cmath>

template<typename T> struct vec2 {
	union {
		struct { T x, y; };
		struct { T a, b; };
		T v[2];
	};

	vec2() { x = y = T(0); }
	vec2(T _x, T _y) { x = _x; y = _y; }
	vec2(T _v)       { x = _v; y = _v; }

	inline const vec2& operator + () const { return *this; }
	inline       vec2  operator - () const { return vec2(-x, -y); }
	inline T           operator [](int i) const { return v[i]; }
	inline T&          operator [](int i)       { return v[i]; }

	inline vec2& operator += (const vec2& v) { x += v.x; y += v.y; return *this; }
	inline vec2& operator -= (const vec2& v) { x -= v.x; y -= v.y; return *this; }
	inline vec2& operator *= (const vec2& v) { x *= v.x; y *= v.y; return *this; }
	inline vec2& operator /= (const vec2& v) { x /= v.x; y /= v.y; return *this; }
	inline vec2& operator *= (const T t)     { x *=   t; y *=   t; this; }
	inline vec2& operator /= (const T t)     { x /=   t; y /=   t; this; }

	inline T len2() const { return x*x + y*y; }
	inline T len()  const { return std::sqrt<T>(x*x + y*y); }

	inline vec2<T> unit() const { T l = len(); return vec2<T>(x/l, y/l); }
	inline void make_unit() { T l = len(); x /= l; y /= l; }
};
template<typename T> inline vec2<T> operator + (const vec2<T>& v0, const vec2<T>& v1) { return vec2<T>(v0.x + v1.x, v0.y + v1.y); }
template<typename T> inline vec2<T> operator - (const vec2<T>& v0, const vec2<T>& v1) { return vec2<T>(v0.x - v1.x, v0.y - v1.y); }
template<typename T> inline vec2<T> operator * (const vec2<T>& v0, const vec2<T>& v1) { return vec2<T>(v0.x * v1.x, v0.y * v1.y); }
template<typename T> inline vec2<T> operator / (const vec2<T>& v0, const vec2<T>& v1) { return vec2<T>(v0.x / v1.x, v0.y / v1.y); }
template<typename T> inline vec2<T> operator * (T t, const vec2<T>& v) { return vec2<T>(t*v.x, t*v.y); }
template<typename T> inline vec2<T> operator * (const vec2<T>& v, T t) { return vec2<T>(t*v.x, t*v.y); }
template<typename T> inline vec2<T> operator / (const vec2<T>& v, T t) { return vec2<T>(v.x/t, v.y/t); }
template<typename T> inline T dot(const vec2<T>& v0, const vec2<T>& v1) { return v0.x*v1.x + v0.y*v1.y; }
template<typename T> inline vec2<T> lerp(const vec2<T>& v0, T t, const vec2<T>& v1) { return (1.0f - t)*v0 + t*v1; }

typedef vec2<float> vec2f;
typedef vec2<int> vec2i;
