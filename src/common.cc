#include "common.h"

const float pi = acosf(-1.0f);

string load_file(const char* file) {
	string contents;
	FILE* fp = fopen(file, "rb");
	if(fp) {
		DEFER(fclose(fp));
		fseek(fp, 0, SEEK_END);
		size_t size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		contents.resize(size);
		fread(&contents[0], size, 1, fp);
	}
	return contents;
}

