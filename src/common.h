#pragma once

#include "defer.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <string>
using namespace std;

#define EPSILON 0.000001
#define LENGTH(a) (sizeof(a)/sizeof(*a))
#define LINE { fprintf(stderr, "@%d\n", __LINE__); }

#ifndef ROGUE_DEBUG
#define asshurt(c) \
	if(c) { (void)(0); }
#else
#define asshurt(c) ((c) ? (void)0 : _asshurt(#c, __FILE__, __FUNCTION__, __LINE__))
#endif

static inline void _asshurt(const char* condition, const char* file, const char* function, int line) {
	fprintf(stderr, "\ncondition %s failed in function \"%s\" on line %d in %s\n", condition, function, line, file);
	exit(1);
}

extern const float pi;
static inline float rad(float deg) { return deg*pi/180.0f; }

string load_file(const char* file);
