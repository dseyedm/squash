#define PF(v)    printf(#v " %f\n", v);
#define PVEC2(v) printf(#v " %f %f\n", v.x, v.y);

#include "ezglext.h"
#include "vec.h"
#include "mat.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <GLFW/glfw3.h>
#include <vector>
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include <sstream>
using namespace std;

float minf(float a, float b) { return b < a ? b : a; }
float maxf(float a, float b) { return b > a ? b : a; }
float clampf(float min, float x, float max) { x = minf(max, x); x = maxf(min, x); return x; }

#define NEW_UNIFORM(S, U) \
	GLuint S##_##U = IHANDLE

program_ext_t blit;
NEW_UNIFORM(blit, color_texture);
NEW_UNIFORM(blit, transform);
NEW_UNIFORM(blit, uv_offset);
NEW_UNIFORM(blit, uv_scale);

program_ext_t postfx;
NEW_UNIFORM(postfx, color_texture);

program_ext_t* programs[] = { &blit, &postfx };
#undef NEW_UNIFORM

texture2d_t color_a;
fbo_t fbo_a;

struct bitmap_t {
	texture2d_t t;
	int w = 0, h = 0; 
	int n_components = 0;
};

struct tilemap_t {
	const char* b_src = NULL;
	bitmap_t b;
	uint32_t tile_w = 0, tile_h = 0;
	
	tilemap_t(
		const char* _b_src,
		uint32_t _tile_w, 
		uint32_t _tile_h)
	{
		b_src = _b_src;
		tile_w = _tile_w;
		tile_h = _tile_h;
	}
};

tilemap_t tilemap_db[] = {
	tilemap_t("tiles.png", 4, 4),
	tilemap_t("players.png", 4, 8),
};

vector<uint16_t> s_unique_tile_index;
union tile_t {
	struct {
		uint16_t i;
		uint16_t tilemap_db;
	};
	uint32_t guid = (uint32_t)-1;
	
	tile_t() { }
	tile_t(uint16_t _tilemap_db, uint16_t _i) { tilemap_db = _tilemap_db; i = _i; }
	void setup(uint16_t _tilemap_db) {
		tilemap_db = _tilemap_db;
		while((size_t)_tilemap_db >= s_unique_tile_index.size()) s_unique_tile_index.push_back(0);
		i = s_unique_tile_index[_tilemap_db]++;
	}
};

float g_player_rotation = 0.0f;
float g_player_scale = 0.0f;
vec2f cam_zoom(0.125f, 0.125f);
vec2f cam_center(0.0f, 0.0f);
float cam_rotation = 0.0f;
const char* level_path = NULL;
vector<vector<tile_t> > level;
vec2f player_bounds(1.2f, 2.4f);
float acc_gravity = 90.0f;
float vel_max_fall = 120.0f;
float vel_max_jump = -40.0f;
float vel_max_leftright = 50.0f;
float acc_leftright = 200.0f;
float rest_leftright = -0.4f;
float rest_updown = -0.2f;
float impulse_jump = -40.0f;
float impulse_dart =  90.0f;
float impulse_leftright = 30.0f;
float friction_leftright = 0.7f;
float ground_thresh = 1.2f;
float wall_thresh = 1.2f;
int max_airjumps = 1;
float player_spawn_time = 5.0f;
tile_t tile_hazard;
tile_t tile_wall;
tile_t tile_spawn_one;
tile_t tile_spawn_two;
struct player_t {
	vec2f pos;
	vec2f vel;
	int airjumps = 0;
	double spawn_time = -1.0;
	bool alive = true;
	bool in_dart = false;
	
	struct control_t { bool up = false, down = false, left = false, right = false; };
	control_t lc;
	static const int n_ticks = 5;
	void tick_impl(float dts, const control_t& c) {
		if(colliding(pos, tile_hazard)) {
			alive = false;
			spawn_time = glfwGetTime() + player_spawn_time;
			return;
		}
		
		vec2f v = vel;
		float acc_amt = dts*acc_leftright;
		if(c.up && !lc.up) {
			lc.up = true;
			bool on_ground = 
				colliding(pos, tile_wall, 0.5f, 0.5f*ground_thresh);
			bool on_right = 
				colliding(pos, tile_wall, 0.5f, 0.5f, 0.5f, 0.5f*wall_thresh);
			bool on_left = 
				colliding(pos, tile_wall, 0.5f, 0.5f, 0.5f*wall_thresh, 0.5f);
			if(on_ground || on_left || on_right) {
				airjumps = 0;
				v.y = impulse_jump;
				if(on_left) {
					v.x = impulse_leftright;
				}
				if(on_right) {
					v.x = -impulse_leftright;
				}
			} else if(airjumps < max_airjumps) {
				++airjumps;
				v.y = impulse_jump;
			}
		}
		if(c.down && !lc.down && !in_dart) {
			in_dart = true;
			lc.down = true;
			v.y += impulse_dart;
		}
		if(c.left && !c.right) {
			v.x -= acc_amt;
		} else if(c.right) {
			v.x += acc_amt;
		}
		
		v.x += -dts*friction_leftright*vel.x;
		
		v.y = clampf(vel_max_jump, v.y, vel_max_fall);
		v.x = clampf(-vel_max_leftright, v.x, vel_max_leftright);
		{
			v.y = v.y + acc_gravity*dts;
			vec2f pp = pos + vec2f(0.0f, v.y*dts);
			if(colliding(pp)) {
				in_dart = false;
				v.y *= rest_updown;
			}
		}
		{
			vec2f pp = pos + vec2f(v.x*dts, 0.0f);
			if(colliding(pp)) {
				v.x *= rest_leftright;
			}
		}

		vel = v;
		pos += vel*dts;
	}
	void tick(float dts, const control_t& c) {
		for(int i = 0; i < n_ticks; ++i) {
			tick_impl(dts/(float)n_ticks, c);
			if(!alive) return;
		}
		lc = c;
	}
	bool colliding(const vec2f& pp, tile_t cooltile = tile_wall, float thup = 0.5f, float thdown = 0.5f, float thleft = 0.5f, float thright = 0.5f, int* out_i = NULL, int* out_j = NULL) {
		float player_top = pp.y - player_bounds.y*thup;
		float player_bottom = pp.y + player_bounds.y*thdown;
		float player_left = pp.x - player_bounds.x*thleft;
		float player_right = pp.x + player_bounds.x*thright;
		
		for(size_t i = 0; i < level.size(); ++i) {
			for(size_t j = 0; j < level[i].size(); ++j) {
				tile_t t = level[i][j];
				if(t.guid != cooltile.guid) continue;
				float tile_top = (float)i - 0.5f;
				float tile_bottom = (float)i + 0.5f;
				float tile_left = (float)j - 0.5f;
				float tile_right = (float)j + 0.5f;
				if(
					player_top < tile_bottom && player_bottom > tile_top &&
					player_left < tile_right && player_right > tile_left)
				{
					if(out_i) *out_i = i;
					if(out_j) *out_j = j;
					return true;
				}
			}
		}
		return false;
	}
};
player_t p0;
player_t p1;
const player_t* game_won_by = NULL;

void reload_level() {
	level.clear();
	asshurt(level_path);
	string level_data = load_file(level_path);
	asshurt(level_data.size());
	stringstream ss; ss << level_data;
	string key;
#define READ_V2(variable) asshurt(ss >> key >> variable.x >> variable.y); getline(ss, key)
#define READ_F(variable) asshurt(ss >> key >> variable); getline(ss, key)
	READ_V2(cam_center);
	READ_F(cam_rotation);
	READ_V2(cam_zoom);
	READ_V2(player_bounds);
	READ_F(acc_gravity);
	READ_F(vel_max_fall);
	READ_F(vel_max_jump);
	READ_F(vel_max_leftright);
	READ_F(acc_leftright);
	READ_F(rest_leftright);
	READ_F(rest_updown);
	READ_F(impulse_jump);
	READ_F(impulse_dart);
	READ_F(impulse_leftright);
	READ_F(friction_leftright);
	READ_F(ground_thresh);
	READ_F(wall_thresh);
	READ_F(max_airjumps);
	READ_F(player_spawn_time);
#undef READ_V2
#undef READ_F
	
	getline(ss, key);
	asshurt(key.size());
	
	string line;
	while(getline(ss, line)) {
		size_t m;
		for(m = 0; m < line.size(); ++m) {
			if(!isspace(line[m])) break;
		}
		if(m >= line.size()) break;
		
		level.emplace_back();
		level.back().reserve(line.size());
		for(size_t i = 0; i < line.size(); ++i) {
			if(isspace(line[i])) continue;
			uint16_t keyi;
			for(keyi = 0; keyi < (uint16_t)key.size(); ++keyi) {
				if(key[keyi] == line[i]) break;
			}
			if(keyi >= (uint16_t)key.size()) keyi = 0;
			
			level[level.size() - 1].push_back(tile_t(0, keyi));
		}
	}
	asshurt(level.size());
}

vec2i resolution(1920/2, 1080/2);
void reload_buffers() {
	color_a.create();
	color_a.bind();
	color_a.set_filter(GL_NEAREST);
	color_a.set_wrap(GL_REPEAT);
	color_a.upload(NULL, resolution.x, resolution.y, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
	color_a.unbind();

	fbo_a.create();
	fbo_a.bind();
	fbo_a.attach(color_a, GL_COLOR_ATTACHMENT0);
	asshurt(fbo_a.status() == GL_FRAMEBUFFER_COMPLETE);
	fbo_a.unbind();
}

void reload_bitmaps() {
	stbi_set_flip_vertically_on_load(true);
	uint8_t* data;
	for(size_t i = 0; i < LENGTH(tilemap_db); ++i) {
		tilemap_t* tm = &tilemap_db[i];
		data = stbi_load(
			tm->b_src, 
			&tm->b.w, 
			&tm->b.h, 
			&tm->b.n_components, 
			STBI_rgb_alpha
		);
		asshurt(tm->b.n_components == 3 || tm->b.n_components == 4);
		if(data) {
			tm->b.t.create();
			tm->b.t.bind();
			tm->b.t.upload(&data[0], tm->b.w, tm->b.h, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
			tm->b.t.set_filter(GL_NEAREST);
			tm->b.t.set_wrap(GL_REPEAT);
			tm->b.t.unbind();
			stbi_image_free(data);
		} else {
			fprintf(stderr, "failed to load tilemap texture %s\n", tm->b_src);
		}
	}
}

struct common_attribute_t {
	const char* name;
	GLenum type;
	GLuint location;
};
enum {
	LOCATION_VERTEX,
	LOCATION_TEXCOORD,
};
const common_attribute_t common_attributes[] = {
	{ "vertex", GL_FLOAT_VEC2, LOCATION_VERTEX },
	{ "texcoord", GL_FLOAT_VEC2, LOCATION_TEXCOORD },
};

#define GET_UNIFORM(S, U) \
	S##_##U = (GLuint)gl(GetUniformLocation(S, #U)); \
	if(S##_##U == IHANDLE) { fprintf(stderr, "warning: '" #S "' shader does not contain uniform '" #U "'\n"); }
struct common_uniform_direct_t {
	const char* name;
	GLenum type;
	void* data;
};
void reload_shaders(const common_uniform_direct_t* commons, int n_commons) {
	{
		program_t candidate_blit; candidate_blit.create();
		if(!program_load_files(&candidate_blit, "blit.vert", "blit.frag")) {
			candidate_blit.destroy();
		} else {
			blit.destroy();
			blit = candidate_blit;
			GET_UNIFORM(blit, color_texture);
			GET_UNIFORM(blit, transform);
			GET_UNIFORM(blit, uv_offset);
			GET_UNIFORM(blit, uv_scale);
		}
	}
	{
		program_t candidate_postfx; candidate_postfx.create();
		if(!program_load_files(&candidate_postfx, "postfx.vert", "postfx.frag")) {
			candidate_postfx.destroy();
		} else {
			postfx.destroy();
			postfx = candidate_postfx;
			GET_UNIFORM(postfx, color_texture);
		}
	}
	for(int p = 0; p < (int)LENGTH(programs); ++p) {
		program_data_t& pd = programs[p]->data;
		pd.create(programs[p]);
		for(size_t u = 0; u < pd.uniforms.size(); ++u) {
			program_uniform_t& uf = pd.uniforms[u];
			for(int c = 0; c < n_commons; ++c) {
				const common_uniform_direct_t& cm = commons[c];
				if(cm.type == uf.type && cm.name == uf.name) {
					uf.data = cm.data;
				}
			}
		}
	}
}
#undef GET_UNIFORM

void glfw_framebuffer_size_changed(GLFWwindow* window, int width, int height) {
	if(width <= 0) width = 1;
	if(height <= 0) height = 1;
	if(width == resolution.x && height == resolution.y) return;
	gl(Viewport(0, 0, resolution.x = width, resolution.y = height));
	reload_buffers();
}

// todo: cache this data.
void get_tile_uvs(const tilemap_t& tm, uint32_t i, vec2f* out_uv_offset, vec2f* out_uv_scale) {
	uint32_t horizontal_x = i*tm.tile_w;
	uint32_t px_x = (horizontal_x % tm.b.w);
	uint32_t px_y = (horizontal_x / tm.b.w)*tm.tile_h;
	*out_uv_offset = vec2f(
		(float)px_x / (float)tm.b.w,
		(float)px_y / (float)tm.b.h);
	*out_uv_scale = vec2f(
		(float)tm.tile_w / (float)tm.b.w,
		(float)tm.tile_h / (float)tm.b.h);
}

void spawn(player_t* p, tile_t in_t) {
	for(size_t i = 0; i < level.size(); ++i) {
		for(size_t j = 0; j < level[i].size(); ++j) {
			tile_t t = level[i][j];
			if(in_t.guid == t.guid) {
				*p = player_t();
				p->pos.x = j;
				p->pos.y = i;
				return;
			} 
		}
	}
}

void respawn_players() {
	p0 = player_t();
	p1 = player_t();
	spawn(&p0, tile_spawn_one);
	spawn(&p1, tile_spawn_two);
}

int main(const int argc, const char* argv[]) {
	if(argc < 2) {
		asshurt(false);
	}
	level_path = argv[1];
	
	srand(time(NULL));
	
	asshurt(glfwInit());
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	GLFWwindow* window = glfwCreateWindow(resolution.x, resolution.y, "Hello World", NULL, NULL); asshurt(window);
	DEFER(glfwDestroyWindow(window));
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, glfw_framebuffer_size_changed);
	glewExperimental = GL_TRUE;
	asshurt(glewInit() == 0); {
		printf("Renderer: %s\n", glGetString(GL_RENDERER));
		printf("OpenGL version %s\n", glGetString(GL_VERSION));

		float now_time, dts, cursor_x, cursor_y;
		const common_uniform_direct_t common_direct_uniforms[] = {
			{ "cursor_x", GL_FLOAT, &cursor_x },
			{ "cursor_y", GL_FLOAT, &cursor_y },
			{ "time", GL_FLOAT, &now_time },
			{ "resolution", GL_INT_VEC2, &resolution },
		};

		reload_shaders(common_direct_uniforms, LENGTH(common_direct_uniforms));
		reload_bitmaps(); DEFER(
			for(size_t i = 0; i < LENGTH(tilemap_db); ++i) {
				tilemap_t* tm = &tilemap_db[i];
				tm->b.t.destroy();
			}
		);
		reload_buffers();
		DEFER(
			for(int z = 0; z < (int)LENGTH(programs); ++z) {
				programs[z]->destroy();
			}
			color_a.destroy();
			fbo_a.destroy();
		);

		const vec2f quad_vertices[] = {
			{ -1.0f, -1.0f, },
			{  1.0f, -1.0f, },
			{ -1.0f,  1.0f, },
			{  1.0f,  1.0f, },
		};
		vbo_t quad_vbo; quad_vbo.create(); DEFER(quad_vbo.destroy());
		quad_vbo.bind(GL_ARRAY_BUFFER);
		gl(BufferData(
			GL_ARRAY_BUFFER,
			sizeof(quad_vertices),
			quad_vertices,
			GL_STATIC_DRAW
		));

		vao_t quad_vao; quad_vao.create(); DEFER(quad_vao.destroy());
		quad_vao.bind();
		gl(EnableVertexAttribArray(0));
		gl(BindBuffer(GL_ARRAY_BUFFER, quad_vbo));
		for(int o = 0; o < (int)LENGTH(programs); ++o) {
			program_ext_t& pr = *programs[o];
			for(size_t e = 0; e < pr.data.attributes.size(); ++e) {
				program_attribute_t& at = pr.data.attributes[e];
				for(int i = 0; i < (int)LENGTH(common_attributes); ++i) {
					const common_attribute_t& ca = common_attributes[i];
					if(ca.type == at.type && ca.name == at.name) {
						gl(VertexAttribPointer(at.handle, 2, GL_FLOAT, GL_FALSE, 0, NULL));
					}
				}
			}
		}
		quad_vao.unbind();
	
#define NEW_TILE(t) tile_t t; t.setup(0);
		NEW_TILE(tile_none);
		tile_spawn_one.setup(0);
		tile_spawn_two.setup(0);
		tile_wall.setup(0);
		tile_hazard.setup(0);
#undef NEW_TILE
#define NEW_TILE(t) tile_t t; t.setup(1);
		NEW_TILE(tile_player_none);
		NEW_TILE(tile_player0);
		NEW_TILE(tile_player1);
#undef NEW_TILE
		reload_level();
		respawn_players();
		
		do {
			// events
			glfwPollEvents();

			{
				now_time = (float)glfwGetTime();
				static bool did_fps = false;
				static float last_time = now_time - 1.0f/60.0f;
				dts = now_time - last_time;
				last_time = now_time;
				if(!did_fps && fmod(now_time, 2.0f) < 0.5f) {
					char title[1024] = { 0 };
					snprintf(title, LENGTH(title), "fps: %.1f\n", 1.0/dts);
					glfwSetWindowTitle(window, title);
					did_fps = true;
				} else {
					did_fps = fmod(now_time, 2.0f) < 0.5f;
				}
			}

			player_t::control_t p0c;
			player_t::control_t p1c;
			bool press_reload         = glfwGetKey(window, GLFW_KEY_R);
			p0c.up          = glfwGetKey(window, GLFW_KEY_W);
			p0c.down        = glfwGetKey(window, GLFW_KEY_S);
			p0c.left        = glfwGetKey(window, GLFW_KEY_A);
			p0c.right       = glfwGetKey(window, GLFW_KEY_D);
			p1c.up          = glfwGetKey(window, GLFW_KEY_U);
			p1c.down        = glfwGetKey(window, GLFW_KEY_J);
			p1c.left        = glfwGetKey(window, GLFW_KEY_H);
			p1c.right       = glfwGetKey(window, GLFW_KEY_K);
			bool press_cam_up         = glfwGetKey(window, GLFW_KEY_UP);
			bool press_cam_down       = glfwGetKey(window, GLFW_KEY_DOWN);
			bool press_cam_left       = glfwGetKey(window, GLFW_KEY_LEFT);
			bool press_cam_right      = glfwGetKey(window, GLFW_KEY_RIGHT);
			bool press_cam_rotate_cw  = glfwGetKey(window, GLFW_KEY_P);
			bool press_cam_rotate_ccw = glfwGetKey(window, GLFW_KEY_O);
			bool press_cam_zoom_in    = glfwGetKey(window, GLFW_KEY_N);
			bool press_cam_zoom_out   = glfwGetKey(window, GLFW_KEY_M);
			
			if(game_won_by == NULL) {
				g_player_rotation = 0.0f;
				g_player_scale = 1.0f;
				if(p0.alive) p0.tick(dts, p0c);
				else if(glfwGetTime() >= p0.spawn_time) spawn(&p0, tile_spawn_one);
				if(p1.alive) p1.tick(dts, p1c);
				else if(glfwGetTime() >= p1.spawn_time) spawn(&p1, tile_spawn_two);
				if(p0.alive && p1.alive) {
					float p0_top = p0.pos.y - player_bounds.y*0.5f;
					float p0_bot = p0.pos.y + player_bounds.y*0.5f;
					float p0_left = p0.pos.x - player_bounds.x*0.5f;
					float p0_right = p0.pos.x + player_bounds.x*0.5f;
					float p1_top = p1.pos.y - player_bounds.y*0.5f;
					float p1_bot = p1.pos.y + player_bounds.y*0.5f;
					float p1_left = p1.pos.x - player_bounds.x*0.5f;
					float p1_right = p1.pos.x + player_bounds.x*0.5f;
					if(
						p0_top < p1_bot && p0_bot > p1_top &&
						p0_left < p1_right && p0_right > p1_left)
					{
						if(p0.in_dart && !p1.in_dart) {
							p1.alive = false;
							p1.spawn_time = glfwGetTime() + player_spawn_time;
						} else if(!p0.in_dart && p1.in_dart) {
							p0.alive = false;
							p0.spawn_time = glfwGetTime() + player_spawn_time;
						}
					}
				} else {
					const player_t* p = NULL;
					tile_t winning_tile;
					if(p0.alive) {
						p = &p0;
						winning_tile = tile_spawn_two;
					} else {
						p = &p1;
						winning_tile = tile_spawn_one;
					} 
					float player_top = p->pos.y - player_bounds.y;
					float player_bottom = p->pos.y + player_bounds.y;
					float player_left = p->pos.x - player_bounds.x;
					float player_right = p->pos.x + player_bounds.x;
					for(size_t i = 0; i < level.size(); ++i) {
						for(size_t j = 0; j < level[i].size(); ++j) {
							tile_t t = level[i][j];
							if(t.guid != winning_tile.guid) continue;
							float tile_top = (float)i - 0.5f;
							float tile_bottom = (float)i + 0.5f;
							float tile_left = (float)j - 0.5f;
							float tile_right = (float)j + 0.5f;
							if(
								player_top < tile_bottom && player_bottom > tile_top &&
								player_left < tile_right && player_right > tile_left)
							{
								game_won_by = p;
							}
						}
					}
				}
			} else {
				g_player_rotation = glfwGetTime();
				g_player_scale = 5.0f*cosf(glfwGetTime());
			}
			
			{
				bool now_cam_changed = 
					press_cam_up || press_cam_down || press_cam_left || press_cam_right || press_cam_rotate_ccw ||
					press_cam_rotate_cw || press_cam_zoom_in || press_cam_zoom_out;
				static bool last_cam_changed = false;
				if(last_cam_changed && !now_cam_changed) {
					PVEC2(cam_center);
					PF(cam_rotation);
					PVEC2(cam_zoom);
				}
				last_cam_changed = now_cam_changed;
			}
			{
				float cam_speed = 10.0f*dts;
				if(press_cam_up)    cam_center.y -= cam_speed;
				if(press_cam_down)  cam_center.y += cam_speed;
				if(press_cam_left)  cam_center.x -= cam_speed;
				if(press_cam_right) cam_center.x += cam_speed;
			}
			{
				float cam_rotate_speed = pi*dts;
				if(press_cam_rotate_ccw) cam_rotation += cam_rotate_speed;
				if(press_cam_rotate_cw)  cam_rotation -= cam_rotate_speed;
			}
			{
				float cam_zoom_speed = 0.50f*dts;
				if(press_cam_zoom_in)  cam_zoom += cam_zoom*vec2f(cam_zoom_speed);
				if(press_cam_zoom_out) cam_zoom -= cam_zoom*vec2f(cam_zoom_speed);
			}

			{
				double dcursor_x, dcursor_y;
				glfwGetCursorPos(window, &dcursor_x, &dcursor_y);
				cursor_x = dcursor_x; cursor_y = dcursor_y;
			}
			
			// render to fbo_a
			fbo_a.bind();
			gl(Disable(GL_DEPTH_TEST));
			gl(Disable(GL_CULL_FACE));
			gl(Enable(GL_BLEND)); gl(BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)); 
			gl(ClearColor(0.0f, 0.0f, 0.0f, 1.0f));
			gl(Clear(GL_COLOR_BUFFER_BIT));

			// blit the level
			mat3f cam_zoom_t = mat3f::scale(
				cam_zoom.x,
				cam_zoom.y
			);
			mat3f cam_rotate_t = mat3f::rotate(-cam_rotation);
			quad_vao.bind();
			blit.bind();
			blit.data.set_uniforms();
			{
				tilemap_t& source = tilemap_db[0];
				if(blit_color_texture != IHANDLE) gl(Uniform1i(blit_color_texture, source.b.t.bind(0)));
				for(size_t q = 0; q < level.size(); ++q) {
					for(size_t w = 0; w < level[q].size(); ++w) {
						mat3f transform =
						mat3f::translate((float)w - cam_center.x, cam_center.y - (float)q)*
						cam_zoom_t*
						cam_rotate_t;
						tile_t t = level[q][w];
						vec2f uv_offset, uv_scale; get_tile_uvs(source, t.i, &uv_offset, &uv_scale);
						if(blit_uv_offset != IHANDLE)     gl(Uniform2f(blit_uv_offset, uv_offset.x, uv_offset.y));
						if(blit_uv_scale != IHANDLE)      gl(Uniform2f(blit_uv_scale, uv_scale.x, uv_scale.y));
						if(blit_transform != IHANDLE)     gl(UniformMatrix3fv(blit_transform, 1, GL_FALSE, &transform.m[0][0]));
						gl(DrawArrays(GL_TRIANGLE_STRIP, 0, LENGTH(quad_vertices)));
					}
				}
			}
			{
				const player_t* players[] = { &p0, &p1 };
				const tile_t player_tiles[] = { tile_player0, tile_player1 };
				tilemap_t& source = tilemap_db[1];
				for(int i = 0; i < LENGTH(players); ++i) {
					const player_t& p = *players[i];
					if(!p.alive) continue;
					const tile_t t = player_tiles[i];
					mat3f transform =
					mat3f::scale(g_player_scale * player_bounds)*
					mat3f::rotate(g_player_rotation)*
					mat3f::translate(p.pos.x - cam_center.x, cam_center.y - p.pos.y)*
					cam_zoom_t*
					cam_rotate_t;
					vec2f uv_offset, uv_scale; get_tile_uvs(source, t.i, &uv_offset, &uv_scale);
					if(blit_color_texture != IHANDLE) gl(Uniform1i(blit_color_texture, source.b.t.bind(0)));
					if(blit_uv_offset != IHANDLE)     gl(Uniform2f(blit_uv_offset, uv_offset.x, uv_offset.y));
					if(blit_uv_scale != IHANDLE)      gl(Uniform2f(blit_uv_scale, uv_scale.x, uv_scale.y));
					if(blit_transform != IHANDLE)     gl(UniformMatrix3fv(blit_transform, 1, GL_FALSE, &transform.m[0][0]));
					gl(DrawArrays(GL_TRIANGLE_STRIP, 0, LENGTH(quad_vertices)));
				}
			}
			blit.unbind();
			quad_vao.unbind();

			fbo_t::unbind();
			gl(Disable(GL_DEPTH_TEST));
			gl(Disable(GL_CULL_FACE));
			gl(ClearColor(0.0, 0.0, 0.0, 1.0));
			gl(Clear(GL_COLOR_BUFFER_BIT));

			// postfx (to screen)
			quad_vao.bind();
			postfx.bind();
			postfx.data.set_uniforms();
			if(postfx_color_texture != IHANDLE) gl(Uniform1i(postfx_color_texture, color_a.bind(0)));
			gl(DrawArrays(GL_TRIANGLE_STRIP, 0, LENGTH(quad_vertices)));
			color_a.unbind(0);
			postfx.unbind();
			quad_vao.unbind();

			glfwSwapBuffers(window);

			{ // reload files
				static bool last_reload = false;
				if(press_reload && !last_reload) {
					reload_shaders(common_direct_uniforms, LENGTH(common_direct_uniforms));
					reload_bitmaps();
					reload_level();
					respawn_players();
					game_won_by = NULL;
					g_player_rotation = 0.0f;
					g_player_scale = 1.0f;
				}
				last_reload = press_reload;
			}
		} while(!glfwWindowShouldClose(window));
	}
	return 0;
}
