#pragma once

#include "common.h"

#include <GL/glew.h>
#include <stdlib.h>

#include <string>
using namespace std;

#define IHANDLE (GLuint)(-1)

#define hprint(v) \
	fprintf(stderr, #v ": %u\n", v.handle);

#ifndef ROGUE_DEBUG
#define gl(OPENGL_CALL) \
	gl##OPENGL_CALL
#else
#define gl(OPENGL_CALL) \
	gl##OPENGL_CALL; \
	{ \
		GLenum gl_error = glGetError(); \
		if(gl_error != GL_NO_ERROR) { \
			const GLubyte* gl_error_str = gluErrorString(gl_error); \
			fprintf(stderr, "\nglGetError() returned %d (%s)", (int)gl_error, gl_error_str); \
			_asshurt(#OPENGL_CALL " == GL_NO_ERROR", __FILE__, __FUNCTION__, __LINE__); \
		} \
	}
#endif

struct handle_t {
	GLuint handle = IHANDLE;
	inline operator GLuint() const { return handle; }
	inline operator GLuint&() { return handle; }
	inline handle_t& operator=(handle_t& other) { asshurt(handle == IHANDLE); handle = other.handle; other.handle = IHANDLE; return *this; }
	virtual void create() { asshurt(false); }
	virtual void destroy() { asshurt(false); }
	~handle_t() { if(handle != IHANDLE) { fprintf(stderr, "warning: leak detected\n"); } }
};
struct shader_t : public handle_t {
	using handle_t::operator =;
	void destroy() override { if(handle != IHANDLE) { gl(DeleteShader(handle)); handle = IHANDLE; } }
	string compile(const char* source);
	string get_source();
};
struct vertex_t : public shader_t {
	using shader_t::operator =;
	void create() override { this->destroy(); handle = gl(CreateShader(GL_VERTEX_SHADER)); }
};
struct fragment_t : public shader_t {
	using shader_t::operator =;
	void create() override { this->destroy(); handle = gl(CreateShader(GL_FRAGMENT_SHADER)); }
};
struct program_t : public handle_t {
	using handle_t::operator =;
	void create() override { this->destroy(); handle = gl(CreateProgram()); }
	void destroy() override { if(handle != IHANDLE) { gl(DeleteProgram(handle)); handle = IHANDLE; } }
	string link(const vertex_t& vertex, const fragment_t& fragment);
	void bind() { gl(UseProgram(handle)); }
	static void unbind() { gl(UseProgram(0)); }
};

bool program_load_files(program_t* program, const char* vertex_path, const char* fragment_path);

struct texture_t : public handle_t {
	using handle_t::operator =;
	void create() override { this->destroy(); gl(GenTextures(1, &handle)); }
	void destroy() override { if(handle != IHANDLE) { gl(DeleteTextures(1, &handle)); handle = IHANDLE; } }
	static GLint active(GLint i) { gl(ActiveTexture(GL_TEXTURE0 + i)); return i; }
};
struct texture2d_t : public texture_t {
	using texture_t::operator =;
	void bind() { gl(BindTexture(GL_TEXTURE_2D, handle)); }
	GLint bind(GLint i) { texture_t::active(i); gl(BindTexture(GL_TEXTURE_2D, handle)); return i; }
	static void unbind() { gl(BindTexture(GL_TEXTURE_2D, 0)); }
	static void unbind(GLint i) { texture_t::active(i); gl(BindTexture(GL_TEXTURE_2D, 0)); }
	void set_filter(GLenum mode) {
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mode));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mode));
	}
	void set_wrap(GLenum mode) {
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mode));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mode));
	}
	void upload(const GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
		gl(TexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, type, data));
	}

};
struct texture3d_t : public texture_t {
	using texture_t::operator =;
	void bind() { gl(BindTexture(GL_TEXTURE_CUBE_MAP, handle)); }
	GLint bind(GLint i) { texture_t::active(i); gl(BindTexture(GL_TEXTURE_CUBE_MAP, handle)); return i; }
	static void unbind() { gl(BindTexture(GL_TEXTURE_CUBE_MAP, 0)); }
	static void unbind(GLint i) { texture_t::active(i); gl(BindTexture(GL_TEXTURE_CUBE_MAP, 0)); }
	void set_filter(GLenum mode) {
		gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, mode));
		gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, mode));
	}
	void set_wrap(GLenum mode) {
		gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, mode));
		gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, mode));
		gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, mode));
	}
	void upload_px(const GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
		gl(TexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, internal_format, width, height, 0, format, type, data));
	}
	void upload_nx(const GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
		gl(TexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, internal_format, width, height, 0, format, type, data));
	}
	void upload_py(const GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
		gl(TexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, internal_format, width, height, 0, format, type, data));
	}
	void upload_ny(const GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
		gl(TexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, internal_format, width, height, 0, format, type, data));
	}
	void upload_pz(const GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
		gl(TexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, internal_format, width, height, 0, format, type, data));
	}
	void upload_nz(const GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
		gl(TexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, internal_format, width, height, 0, format, type, data));
	}
};
struct rbo_t : public handle_t {
	using handle_t::operator =;
	void create() override { this->destroy(); gl(GenRenderbuffers(1, &handle)); }
	void destroy() override { if(handle != IHANDLE) { gl(DeleteRenderbuffers(1, &handle)); handle = IHANDLE; } }
};
struct fbo_t : public handle_t {
	using handle_t::operator =;
	void create() override { this->destroy(); gl(GenFramebuffers(1, &handle)); }
	void destroy() override { if(handle != IHANDLE) { gl(DeleteFramebuffers(1, &handle)); handle = IHANDLE; } }
	void bind() { gl(BindFramebuffer(GL_FRAMEBUFFER, handle)); }
	static void unbind() { gl(BindFramebuffer(GL_FRAMEBUFFER, 0)); }
	void attach(const texture2d_t& t, GLenum attachment) {
		gl(FramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, t, 0));
	}
	void attach(const rbo_t& r, GLenum attachment) {
		gl(FramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, r));
	}
	static GLenum status() { return glCheckFramebufferStatus(GL_FRAMEBUFFER); }
};

struct vbo_t : public handle_t {
	using handle_t::operator =;
	void create() override { this->destroy(); gl(GenBuffers(1, &handle)); }
	void destroy() override { if(handle != IHANDLE) { gl(DeleteBuffers(1, &handle)); handle = IHANDLE; } }
	void bind(GLenum mode) { gl(BindBuffer(mode, handle)); }
	static void unbind(GLenum mode) { gl(BindBuffer(mode, 0));  }
};
struct vao_t : public handle_t {
	using handle_t::operator =;
	void create() override { this->destroy(); gl(GenVertexArrays(1, &handle)); }
	void destroy() override { if(handle != IHANDLE) { gl(DeleteVertexArrays(1, &handle)); handle = IHANDLE; } }
	void bind() { gl(BindVertexArray(handle)); }
	static void unbind() { gl(BindVertexArray(0)); }
};
