#include "ezgl.h"

#include <string>
#include <sstream>
#include <set>
#include <string.h>
using namespace std;

static const char* prefix = "#pragma"; const size_t prefix_strlen = strlen(prefix);
static const char* include = "include";
static const char* vertex_zone = "vertex";
static const char* fragment_zone = "fragment";

string shader_t::compile(const char* source) {
	gl(ShaderSource(handle, 1, &source, NULL));
	gl(CompileShader(handle));
	GLint compiled = GL_FALSE;
	gl(GetShaderiv(handle, GL_COMPILE_STATUS, &compiled));
	string info;
	if(compiled != GL_TRUE) {
		GLint info_size;
		gl(GetShaderiv(handle, GL_INFO_LOG_LENGTH, &info_size));
		info.resize(info_size);
		gl(GetShaderInfoLog(handle, info_size, NULL, &info[0]));
	}
	return info;
}
string shader_t::get_source() {
	GLsizei src_size;
	gl(GetShaderiv(handle, GL_SHADER_SOURCE_LENGTH, &src_size));
	string src; src.resize(src_size);
	gl(GetShaderSource(handle, src_size, NULL, &src[0]));
	return src;
}
string program_t::link(const vertex_t& vertex, const fragment_t& fragment) {
	gl(AttachShader(handle, vertex));
	gl(AttachShader(handle, fragment));
	gl(LinkProgram(handle));
	gl(DetachShader(handle, vertex));
	gl(DetachShader(handle, fragment));
	GLint linked = GL_FALSE;
	gl(GetProgramiv(handle, GL_LINK_STATUS, &linked));
	string info;
	if(linked != GL_TRUE) {
		GLint info_size;
		gl(GetProgramiv(handle, GL_INFO_LOG_LENGTH, &info_size));
		info.resize(info_size);
		gl(GetProgramInfoLog(handle, info_size, NULL, &info[0]));
	}
	return info;
}

bool program_load_files(program_t* program, const char* vertex_path, const char* fragment_path) {
	string content = load_file(vertex_path); 
	if(!content.size()) {
		fprintf(stderr, "failed to find file %s\n", vertex_path);
		return false;
	}
	
	vertex_t vertex; vertex.create(); DEFER(vertex.destroy());
	string result = vertex.compile(content.c_str());
	if(result.size()) {
		fprintf(stderr, "failed to load vertex shader %s:\n%s", vertex_path, result.c_str());
		return false;
	}

	content = load_file(fragment_path);
	if(!content.size()) {
		fprintf(stderr, "failed to find file %s\n", fragment_path);
		return false;
	}
	fragment_t fragment; fragment.create(); DEFER(fragment.destroy());
	result = fragment.compile(content.c_str());
	if(result.size()) {
		fprintf(stderr, "failed to load fragment shader %s:\n%s", fragment_path, result.c_str());
		return false;
	}

	result = program->link(vertex, fragment);
	if(result.size()) {
		fprintf(stderr, "failed to link program from shaders:\n%s\n%s:\n%s", vertex_path, fragment_path, result.c_str());
		return false;
	}
	return true;
}
