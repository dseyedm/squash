#pragma once

#include "vec.h"

#include <cmath>

template<typename T> struct mat3 {
	union {
		struct { 
			T xx, xy, xz;
			T yx, yy, yz;
			T zx, zy, zz;
		};
		struct { 
			T m00, m01, m02;
			T m10, m11, m12;
			T m20, m21, m22;
		};
		T m[3][3];
	};

	mat3() {
		xx=T(1); xy=T(0); xz=T(0);
		yx=T(0); yy=T(1); yz=T(0);
		zx=T(0); zy=T(0); zz=T(1); 
	}
	mat3(
		T _xx, T _xy, T _xz,
		T _yx, T _yy, T _yz,
		T _zx, T _zy, T _zz) {
		xx = _xx; xy = _xy; xz = _xz;
		yx = _yx; yy = _yy; yz = _yz;
		zx = _zx; zy = _zy; zz = _zz;
	}

	inline       T** operator ()()       { return m; }
	inline const T** operator ()() const { return m; }
	
	inline mat3& operator *= (const mat3& m) { *this = *this * m; }
	
	static inline mat3 translate(T x, T y) {
		return mat3<T>(
			T(1), T(0), T(0),
			T(0), T(1), T(0),
			T(x), T(y), T(1)
		);
	}
	static inline mat3 translate(const vec2<T>& v) { return translate(v.x, v.y); }
	// ccw
	static inline mat3 rotate(T a) {
		T s = std::sin(a);
		T c = std::cos(a);
		return mat3<T>(
			   c,    s, T(0),
			  -s,    c, T(0),
			T(0), T(0), T(1)
		);
	}
	static inline mat3 scale(T x, T y) { 
		return mat3<T>(
			   x, T(0), T(0),
			T(0),    y, T(0),
			T(0), T(0), T(1)
		);
	}
	static inline mat3 scale(T s) { return scale(s, s); }
	static inline mat3 scale(const vec2<T>& v) { return scale(v.x, v.y); }
};
template<typename T> inline mat3<T> operator * (const mat3<T>& m0, const mat3<T>& m1) {
	return mat3<T>(
		(m0.xx * m1.xx) + (m0.xy * m1.yx) + (m0.xz * m1.zx),
		(m0.xx * m1.xy) + (m0.xy * m1.yy) + (m0.xz * m1.zy),
		(m0.xx * m1.xz) + (m0.xy * m1.yz) + (m0.xz * m1.zz),

		(m0.yx * m1.xx) + (m0.yy * m1.yx) + (m0.yz * m1.zx),
		(m0.yx * m1.xy) + (m0.yy * m1.yy) + (m0.yz * m1.zy),
		(m0.yx * m1.xz) + (m0.yy * m1.yz) + (m0.yz * m1.zz),

		(m0.zx * m1.xx) + (m0.zy * m1.yx) + (m0.zz * m1.zx),
		(m0.zx * m1.xy) + (m0.zy * m1.yy) + (m0.zz * m1.zy),
		(m0.zx * m1.xz) + (m0.zy * m1.yz) + (m0.zz * m1.zz)
	);
}


typedef mat3<float> mat3f;
