#pragma once

template <typename F> struct scope_exit_t {
	F f;
	scope_exit_t(F f) : f(f) { }
	~scope_exit_t() { f(); }
};
template <typename F> inline scope_exit_t<F> make_scope_exit(F f) { return scope_exit_t<F>(f); };

#define CONCAT_IMPL(x, y) x##y
#define CONCAT(x, y) CONCAT_IMPL(x, y)
#define DEFER(code) \
	auto CONCAT(scope_exit_, __COUNTER__) = make_scope_exit([&](){code;})
