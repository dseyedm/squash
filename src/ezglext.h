#pragma once

#include "ezgl.h"

#include <vector>
#include <string>
using namespace std;

struct program_uniform_t {
	GLuint handle = IHANDLE;
	GLenum type = (GLenum)0;
	string name;
	void* data = NULL;
};
struct program_attribute_t {
	GLuint handle = IHANDLE;
	GLenum type = (GLenum)0;
	string name;
};
struct program_data_t {
	program_t* parent = NULL;
	vector<program_uniform_t> uniforms;
	vector<program_attribute_t> attributes;

	void create(program_t* p);
	void set_uniforms();
};
struct program_ext_t : public program_t {
	using program_t::operator =;
	program_data_t data;
};
