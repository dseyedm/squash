#include "ezglext.h"

#include <vector>

void program_data_t::create(program_t* p) {
	parent = p;
	
	GLint n_uniforms;
	gl(GetProgramiv(*parent, GL_ACTIVE_UNIFORMS, &n_uniforms));
	if(n_uniforms > 0) {
		GLint name_max_size;
		gl(GetProgramiv(*parent, GL_ACTIVE_UNIFORM_MAX_LENGTH, &name_max_size));
		char stack_name[name_max_size];
		uniforms.resize(n_uniforms);
		for(GLint e = 0; e < n_uniforms; ++e) {
			GLsizei this_size;
			GLint size;
			GLenum type;
			gl(GetActiveUniform(*parent, e, name_max_size, &this_size, &size, &type, stack_name));
			uniforms[e].handle = gl(GetUniformLocation(*parent, stack_name));
			uniforms[e].type = type;
			uniforms[e].name = stack_name;
			uniforms[e].data = NULL;
		}
	}
	
	GLint n_attributes;
	gl(GetProgramiv(*parent, GL_ACTIVE_ATTRIBUTES, &n_attributes));
	if(n_attributes > 0) {
		GLint name_max_size;
		gl(GetProgramiv(*parent, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &name_max_size));
		char stack_name[name_max_size];
		attributes.resize(n_attributes);
		for(GLint e = 0; e < n_attributes; ++e) {
			GLsizei this_size;
			GLint size;
			GLenum type;
			gl(GetActiveAttrib(*parent, e, name_max_size, &this_size, &size, &type, stack_name));
			attributes[e].handle = gl(GetAttribLocation(*parent, stack_name));
			attributes[e].type = type;
			attributes[e].name = stack_name;
		}
	}
}
void program_data_t::set_uniforms() {
	for(size_t q = 0; q < uniforms.size(); ++q) {
		program_uniform_t& uf = uniforms[q];
		if(!uf.data) continue;
		switch(uf.type) {
			case GL_FLOAT: {
				const float* d = (const float*)uf.data;
				gl(Uniform1f(uf.handle, *d));
				break;
		       } case GL_FLOAT_VEC2: {
			       const float* d = (const float*)uf.data;
			       gl(Uniform2f(uf.handle, d[0], d[1]));
			       break;
		       } case GL_FLOAT_VEC3: {
				const float* d = (const float*)uf.data;
				gl(Uniform3f(uf.handle, d[0], d[1], d[2]));
				break;
		       } case GL_INT_VEC2: {
				const int* d = (const int*)uf.data;
				gl(Uniform2i(uf.handle, d[0], d[1]));
				break;
		       } case GL_FLOAT_MAT4: {
				const float* d = (const float*)uf.data;
				gl(UniformMatrix4fv(uf.handle, 1, GL_FALSE, &d[0]));
				break;
		       } default: {
				fprintf(stderr, "name = %s, type = %d\n", uf.name, uf.type);
				asshurt(false);
		       }
		}
	}
}
