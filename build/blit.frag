#version 330
in vec2 frag_uv;
uniform sampler2D color_texture;
out vec4 out_frag;
void main() {
	out_frag = texture(color_texture, frag_uv).rgba;
}

