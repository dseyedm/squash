#version 330
in vec2 vertex;
out vec2 frag_uv;
void main() {
	frag_uv = vertex.xy*0.5 + 0.5;
	gl_Position = vec4(vertex, 0.0, 1.0);
}
