#version 330
uniform mat3 transform;
uniform ivec2 resolution;
uniform vec2 uv_offset;
uniform vec2 uv_scale;
in vec2 vertex;
out vec2 frag_uv;
void main() {
	vec3 transformed = transform*vec3(0.5*vertex.xy, 1.0);
	vec2 uv_scale_correct = vec2(uv_scale.x, -uv_scale.y);
	vec2 uv_offset_correct = vec2(uv_offset.x, uv_offset.y);
	frag_uv = uv_offset_correct + uv_scale_correct*(vertex.xy*0.5 + 0.5);
	transformed.x *= float(resolution.y)/float(resolution.x);
	gl_Position = vec4(transformed.xy, 0.0, 1.0);
}
